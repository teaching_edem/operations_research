Simulation
=============================================
This chapter covers some aspects of simulation and its applications in Operations Research.

This chapter contains the resources listed in the following table of contents.

Tutorials
__________
This section contains some basic tutorials:

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Introduction to Simulation </Simulation/Tutorials/Intro to Simulation>


Exercises
__________
In this section you have an example of a simulation solved with the Montecarlo method:

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Commission sales (Easy simulation exercise) </Simulation/Exercises/Sales commissions forecast>

Solved Exercises
__________________
In this section you have the solution to the different exercises.

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Commission sales (Easy simulation exercise) </Simulation/Solved/Sales commissions forecast>
