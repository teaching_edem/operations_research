Non-Linear Programming
=============================================
This chapter provides an introduction to Non-Linear Programming (NLP), the branch of optimisation that deals with problem models where the functions that define the relationship between the unknowns (either objective function or constraints) are not linear.
The fact that the functions are not linear makes NLP a lot harder to deal with from a mathematical point of view. The chapter will focus on specific cases that have many applications in real life problems, particularly quadratic functions and their relationship to machine learning.
This chapter contains the resources listed in the following table of contents.

Tutorials
__________
This section contains some basic tutorials:

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Introduction to Non-Linear Programming </NLP/tutorials/NLP Intro>
    Unconstrained NLP </NLP/tutorials/Unconstrained NLP>
    Gradient descent methods </NLP/tutorials/Gradient Descent Methods>
    Quadratic Optimization </NLP/tutorials/Quadratic Optimization>
    Constrained NLP </NLP/tutorials/Constrained NLP>

Exercises
__________
In this section you have a collection of CLP problems sorted by difficulty:

- **Easy problems**: You can tackle these problems after the first lesson on NLP.
- **Normal problems**: These problems require that you are familiar with concepts like Kuhn Tucker conditions.
- **Hard problems**: This is the type of problems that definitively get you ready for the exam.


.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Planning the construction of homes (easy) </NLP/exercises/Planning the construction of homes>
    Car Production Planning (normal) </NLP/exercises/Car Production Planning>
    Nova Case (normal) </NLP/exercises/Nova Case>
    Maximising investments of a startup </NLP/exercises/Maximising investments of a startup>
    Planning refreshments production </NLP/exercises/Planning refreshments production>
    Production Mix of Pharma </NLP/exercises/Production Mix of Pharma>
    Optimising the design of headphones </NLP/exercises/Banshee limited>

Solved Exercises
__________________
In this section you have the solution to the different exercises.

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:
    :titlesonly:

    Planning the construction of homes (easy) </NLP/solved/Planning the construction of homes (Solved)>
    Car Production Planning (easy) </NLP/solved/Car Production Planning (Solved)>
    Nova Case (normal) </NLP/solved/Nova Case (Solved)>
    Maximising investments of a startup </NLP/solved/Maximising investments of a startup (Solved)>
    Planning refreshments production </NLP/solved/Planning refreshments production (Solved)>
    Production Mix of Pharma </NLP/solved/Production Mix of Pharma>
    Optimising the design of headphones </NLP/solved/Banshee limited>