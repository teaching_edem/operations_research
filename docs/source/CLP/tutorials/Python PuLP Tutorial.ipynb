{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Linear Programming with Python - Part II\n",
    "## Linear Optimisation with PulP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial, we will learn to model and solve Linear Programming Problems using the Python open source Linear Programming library [PuLP](http://pythonhosted.org/PuLP/). PuLP can be installed using Conda, as described [here](https://anaconda.org/conda-forge/pulp).\n",
    "\n",
    "To guide this example, we will use a simple LPP formulated in class:\n",
    "\n",
    "maximise $z = 300x + 250y$\n",
    "\n",
    "Subject to:\n",
    "\n",
    "$2x + y \\leq 40$  \n",
    "$x + 3y \\leq 45$  \n",
    "$x \\leq 12$  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let´s start importing the library PuLP to solve linear programs\n",
    "import pulp\n",
    "# We are going to use panda to display the results as tables using Panda\n",
    "import pandas as pd\n",
    "#And we will use numpy to perform array operations\n",
    "import numpy as np\n",
    "#We will use display and Markdown to format the output of code cells as Markdown\n",
    "from IPython.display import display, Markdown"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem Class LpProblem\n",
    "PuLP uses *classes* providing different methods to model and solve LPPs. The class that will contain our model is the **LpProblem** class. To create a new LpProblem we use the pulp LpProblem function:\n",
    "\n",
    "- **LpProblem(name='None', sense=1):** Creates a new Linear Programming Problem. The parameter name (default 'None') assigns a name to the problem. The parameter sense (either pulp.LpMinimise or pulp.LpMaximize) sets the type of objective function. The default is minimize.\n",
    "\n",
    "Let us create an instance for our problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an instance of the problem class using LpProblem\n",
    "model = pulp.LpProblem(\"Production_Mix_example\", pulp.LpMaximize) #this will create an instance of an LP Maximise problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variable class LpVariable\n",
    "The definition of a LPP program with PuLP is very similar to the standard procedure used to model a problem. First, we need to define the unknown variables in our problem. For this purpose we use the class **LpVariable**. The function LpVariable allows us to create a variable:\n",
    "\n",
    "- **LpVariable(name, lowBound=None, upBound=None, cat='Continuous', e=None):** Creates an instance of variable with the following properties:\n",
    "    - **Name:** The name of the variable to be used in the solution. \n",
    "    - **lowBoud:** The lower bound of the variable, the default is unsrestricted (-Inf).\n",
    "    - **upBound:** The upper bound of the variable. The default is unrestricted (Inf).\n",
    "    - **cat:** Either 'Continuous' for continuous variables, 'Binary' for binary variables or 'Integer' for Integer variables. We will see in detail binary and integer variables in the course unit for Mixed Integer Programming, but now you know that you will be able to model and solve this type of problems with PuLP. The default is 'Continuous'.\n",
    "    - **e:** This parameter is outside the scope of this course and can be neglected for now.\n",
    "\n",
    "We can define the variables of our problem using the LpVariable function:\n",
    "\n",
    "```python\n",
    "x = pulp.LpVariable('x', lowBound=0, cat='Continuous')\n",
    "y = pulp.LpVariable('y', lowBound=0, cat='Continuous')\n",
    "```\n",
    "\n",
    "Note however that using this function, we need a line of code for every unknown. This simply does not scale up. What if we have hundreds of unknowns? Luckily for us, PuLP provides a convenient method to write more efficient codes for our program, the **LpVariable.dicts** method, which basically allows us to create a set of variables with the same category, upper bounds and lower bounds at once:\n",
    "\n",
    "- **LpVariable.dicts(name, indexs, lowBound=None, upBound=None, cat='Continuous')**: Creates a dictionary containing variables of type cat (default 'Continuous'), indexed with the keys contained in the *iterable* index and bounded by lowBound (default -Inf) and upBound (default Inf).\n",
    "\n",
    "For instance, we can write the same code as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First we define a tuple with the variable names x and y\n",
    "variable_names = ('x','y')\n",
    "# Then we create a variable from a dictionary, using the variable names as keys\n",
    "variables = pulp.LpVariable.dicts(\"vars\",\n",
    "                                     (i for i in variable_names),\n",
    "                                     lowBound=0,\n",
    "                                     cat='Continuous')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we have created a tuple with the variable names and then created a dictionary with the actual variables that we will use in our model. We will be able to get the variables from the dictionary using the names as keys. This way, if we had for instance 20 variables, we coud still create them only with two lines of code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding expressions\n",
    "In PuLP, both objective function and constraints are *expressions* (algebraic expressions containing variables) that have to be added to the instance problem using the standard operand '+='. For instance, to add the objective function in this example, we could write:\n",
    "\n",
    "```python\n",
    "    model += 300 * x + 250 * y, \"Profit\"\n",
    "```\n",
    "With this line of code, we have added a new expression with name \"Profit\" that multiplies the technological coefficients to the variables X and Y (as defined in the code snippet in the previous section). This is the simplest way to create a expression, but it is clear that it is not the most scalable way, since we need to add a new term to the summation manually for every variable. PuLP provides a convenient function, *lpSum* to achieve this result programmatically. *lpSum* takes an array of expressions and returns the summation of the elements in the array. Let us see it action:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We define the technological coefficients\n",
    "coefficients = [300, 250]\n",
    "# Then we add the objective function to the model like\n",
    "# model += linear_expression, name\n",
    "# eg model += 300*X + 250y, \"Profit\"\n",
    "# We use the function lpSum to generate the linear expression from a vector\n",
    "# The vector is generated using a for loop over the variable names:\n",
    "model += (\n",
    "    pulp.lpSum([\n",
    "        coefficients[i] * variables[variable_names[i]]\n",
    "        for i in range(len(variable_names))])\n",
    "), \"Profit\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that we have used **list comprehension** to create the array passed to the lpSum function using an index array. In this case we have created an array of length equal to the number of variables using the functions **range** and **length**.\n",
    "We can follow the same method to add constraints. For instance, the simplest way to add the constraint in our example is:\n",
    "```python\n",
    "# And the constraints\n",
    "model += 2 * X + Y <= 40, \"Man Power\"\n",
    "model += X + 3 * Y <= 45, \"Machine Operating Time\"\n",
    "model += X <=12, \"Marketing\"\n",
    "```\n",
    "However, we can see that this is not the most scalable alternative there is, since we will need to add a new line of code every constraint. There is another approach, to put our data in iterable objects and use list comprehension and for loops to define the constraints:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# And the constraints, the Matrix A\n",
    "A=[[2, 1], #Coefficients of the first constraint\n",
    "   [1, 3], #Coefficients of the second constraint\n",
    "   [1, 0]] #Coefficients of the third constraint\n",
    "\n",
    "# And vector b\n",
    "b = [40, 45, 12] #limits of the three constraints\n",
    "\n",
    "# need We also define the name for the constraints\n",
    "constraint_names = ['Man Power', 'Machine Operating Time', 'Marketing']\n",
    "# Now we add the constraints using\n",
    "# model += expression, name\n",
    "# eg model += 2*X + y <= 40\n",
    "# We add all constraints in a loop, using a vector and the function lpSum to generate the linear expression:\n",
    "for i in range(len(A)):           \n",
    "    model += pulp.lpSum([\n",
    "        A[i][j] * variables[variable_names[j]] \n",
    "        for j in range(len(variable_names))]) <= b[i] , constraint_names[i]\n",
    "\n",
    "#note that in this case all constraints are of type less or equal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have created our model, we can get the solution just by calling the method **solve()**. The status of the solution can be read in the LpStatus attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Optimal'"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Solve our problem\n",
    "model.solve()\n",
    "pulp.LpStatus[model.status]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let us display the solution in a nice table using Pandas. We are going to first display the solution value using markdown and then we will use Pandas to create a table with the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "The value of the objective function is **6350.00**"
      ],
      "text/plain": [
       "<IPython.core.display.Markdown object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/markdown": [
       "The following tables show the values obtained: "
      ],
      "text/plain": [
       "<IPython.core.display.Markdown object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Variables</th>\n",
       "      <th>Solution</th>\n",
       "      <th>Reduced cost</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <td>x</td>\n",
       "      <td>vars_x</td>\n",
       "      <td>12.00</td>\n",
       "      <td>0.00</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>y</td>\n",
       "      <td>vars_y</td>\n",
       "      <td>11.00</td>\n",
       "      <td>0.00</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "  Variables Solution Reduced cost\n",
       "x    vars_x    12.00         0.00\n",
       "y    vars_y    11.00         0.00"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Constraint</th>\n",
       "      <th>Right Hand Side</th>\n",
       "      <th>Slack</th>\n",
       "      <th>Shadow Price</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <td>0</td>\n",
       "      <td>Man_Power</td>\n",
       "      <td>40.00</td>\n",
       "      <td>5.00</td>\n",
       "      <td>-0.00</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>1</td>\n",
       "      <td>Machine_Operating_Time</td>\n",
       "      <td>45.00</td>\n",
       "      <td>-0.00</td>\n",
       "      <td>83.33</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>2</td>\n",
       "      <td>Marketing</td>\n",
       "      <td>12.00</td>\n",
       "      <td>-0.00</td>\n",
       "      <td>216.67</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "               Constraint Right Hand Side  Slack Shadow Price\n",
       "0               Man_Power           40.00   5.00        -0.00\n",
       "1  Machine_Operating_Time           45.00  -0.00        83.33\n",
       "2               Marketing           12.00  -0.00       216.67"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Solution\n",
    "max_z = pulp.value(model.objective)\n",
    "\n",
    "#We use display and Mardown to show the value using markdown\n",
    "display(Markdown(\"The value of the objective function is **%.2f**\"%max_z))\n",
    "\n",
    "\n",
    "# Print our decision variable values\n",
    "display(Markdown(\"The following tables show the values obtained: \"))\n",
    "# First we create a dataframe from the dictionary of the solution. We want to use the variable indexes to present the results and \n",
    "# place the different values provided by the solver in the data frame.\n",
    "var_df = pd.DataFrame.from_dict(variables, orient=\"index\", \n",
    "                                columns = [\"Variables\"])\n",
    "# First we add the solution. We apply a lambda function to get only two decimals:\n",
    "var_df[\"Solution\"] = var_df[\"Variables\"].apply(lambda item: \"{:.2f}\".format(float(item.varValue)))\n",
    "# We do the same for the reduced cost:\n",
    "var_df[\"Reduced cost\"] = var_df[\"Variables\"].apply(lambda item: \"{:.2f}\".format(float(item.dj)))\n",
    "\n",
    "\n",
    "# We use the display function to represent the results:\n",
    "display(var_df)\n",
    "\n",
    "\n",
    "# we define a dictionary with the constraints:\n",
    "const_dict = dict(model.constraints)\n",
    "#We create a list of records from the dictionary and exclude the Expression to have a more compact solution. \n",
    "con_df = pd.DataFrame.from_records(list(const_dict.items()), exclude=[\"Expression\"], columns=[\"Constraint\", \"Expression\"])\n",
    "\n",
    "#Now we add columns for the solution, the slack and shadow price\n",
    "\n",
    "con_df[\"Right Hand Side\"] = con_df[\"Constraint\"].apply(lambda item: \"{:.2f}\".format(-const_dict[item].constant))\n",
    "con_df[\"Slack\"] = con_df[\"Constraint\"].apply(lambda item: \"{:.2f}\".format(const_dict[item].slack))\n",
    "con_df[\"Shadow Price\"] = con_df[\"Constraint\"].apply(lambda item: \"{:.2f}\".format(const_dict[item].pi))\n",
    "\n",
    "# And we display the results\n",
    "display(con_df)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "source": [],
    "metadata": {
     "collapsed": false
    }
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}